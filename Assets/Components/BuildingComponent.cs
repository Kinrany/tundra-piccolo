﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingComponent : MonoBehaviour {

	public static Dictionary<BuildingType, GameObject> BuildingTypePrefabs;

	public Building? Building {
		get {
			return _building;
		}
		set {
			_building = value;
			UpdateModel();
		}
	}

	public static BuildingComponent AddToObject(GameObject obj, Building? building) {
		var component = obj.AddComponent<BuildingComponent>();
		component.Building = building;
		return component;
	}


	private Building? _building = null;

	private void UpdateModel() {
		const string MODEL_OBJECT_NAME = "Model";

		// destroy old model if present
		var oldModelTransform = transform.Find(MODEL_OBJECT_NAME);
		if (oldModelTransform != null) {
			Destroy(oldModelTransform.gameObject);
		}

		// if tile isn't empty, create new model
		if (_building != null) {
			var newModelPrefab = BuildingTypePrefabs[((Building)_building).Type];
			var newModelObject = Instantiate(newModelPrefab, gameObject.transform);
			newModelObject.name = MODEL_OBJECT_NAME;
		}
	}
}
