﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class TileButtonsComponent : MonoBehaviour {

	public int FontSize = 20;

	public TileButtonsComponent() :base() {
		buildingButtons = new TileButtons();
	}

	private TileButtons buildingButtons;
	private SelectionComponent Selector;

	private void Start() {
		var selectionManager = FindObjectOfType<SelectionManager>();
		Selector = selectionManager.Orange;
	}

	private void Update() {
		var building = Selector.BuildingComponent;
		if (building == null) {
			return;
		}

		var gridPosition = building.GetComponent<GridPositionComponent>();
		foreach (var button in buildingButtons) {
			if (button.Filter(gridPosition) && Input.GetKeyDown(button.Key)) {
				button.Action(gridPosition);
			}
		}
	}

	private void OnGUI() {
		var building = Selector.BuildingComponent;
		if (building == null) {
			return;
		}

		var gridPosition = building.GetComponent<GridPositionComponent>();

		StringBuilder sb = new StringBuilder();
		foreach (var button in buildingButtons) {
			if (button.Filter(gridPosition)) {
				sb.AppendLine(String.Format("{0}: {1}", button.Key, button.Text));
			}
		}

		string str = sb.ToString().Trim();
		if (str != "") {
			var style = GUI.skin.box;
			style.fontSize = FontSize;
			GUILayout.Box(str, style);
		}
	}
}
