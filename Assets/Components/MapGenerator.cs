﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

	public int Width = 20;
	public int Height = 20;

	public void Generate(int width, int height) {
		var mapManager = FindObjectOfType<MapManager>();
		var rng = new System.Random();

		mapManager.Clear();

		for (int x = 0; x < width; ++x) {
			for (int y = 0; y < height; ++y) {
				var coord = new GridCoord(x, y);

				FloorType floorType = (rng.Next(2) == 0) ? FloorType.Soil : FloorType.Grass;
				mapManager.SetFloor(coord, new Floor(floorType));

				if (rng.Next(3) == 0) {
					mapManager.SetBuilding(coord, new Building(BuildingType.Tree));
				}
			}
		}
	}

	private void Start() {
		Generate(Width, Height);
	}
}
