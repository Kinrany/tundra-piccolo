﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class ShowFPS : MonoBehaviour {

	public Vector2 RectSize = new Vector2(100, 20);

	public const float INTERVAL = 0.3f;

	public Font CourierNew;


	private int frameCount;
	private float timePassed;
	private float lastFps;

	private void Start() {
		frameCount = 0;
		timePassed = 0;
		lastFps = 0;
	}

	private void Update() {
		timePassed += Time.deltaTime;
		if (timePassed > INTERVAL) {
			timePassed = timePassed % INTERVAL;
			lastFps = frameCount / INTERVAL;
			frameCount = 1;
		}
		else {
			frameCount++;
		}
	}

	private void OnGUI() {
		var style = new GUIStyle(GUI.skin.button);
		style.alignment = TextAnchor.MiddleLeft;
		style.font = CourierNew;
		style.fontSize = 24;

		var position = new Vector2(Screen.width - RectSize.x, 0);
		var rect = new Rect(position, RectSize);
		var text = String.Format(CultureInfo.InvariantCulture, "FPS: {0,4:#0.0}", lastFps);
		GUI.Button(rect, text);
	}
}
