﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitOnEsc : MonoBehaviour {

	public KeyCode ExitKey;

	// Update is called once per frame
	void Update() {
		if (Input.GetKey(ExitKey)) {
			Application.Quit();
		}
	}
}
