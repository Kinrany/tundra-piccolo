﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapManager : MonoBehaviour {

	public GameObject PrefabBuilding;
	public GameObject PrefabFloor;

	public GameObject FloorGrass;
	public GameObject FloorSoil;

	public GameObject BuildingTree;
	public GameObject BuildingStump;
	

	public FloorComponent GetFloor(GridCoord coord) {
		FloorComponent floorComponent;
		bool success = floorObjects.TryGetValue(coord, out floorComponent);
		return (success) ? floorComponent : CreateFloor(coord);
	}

	public void SetFloor(GridCoord coord, Floor floor) {
		var component = GetFloor(coord);
		component.Floor = floor;
	}

	public void RemoveFloor(GridCoord coord) {
		var component = GetFloor(coord);
		component.Floor = null;
	}
	

	public BuildingComponent GetBuilding(GridCoord coord) {
		BuildingComponent buildingComponent;
		bool success = buildingObjects.TryGetValue(coord, out buildingComponent);
		return (success) ? buildingComponent : CreateBuilding(coord);
	}
	
	public void SetBuilding(GridCoord coord, Building building) {
		var component = GetBuilding(coord);
		component.Building = building;
	}

	public void RemoveBuilding(GridCoord coord) {
		var component = GetBuilding(coord);
		component.Building = null;
	}


	public void Clear() {
		foreach (var pair in floorObjects) {
			var floor = pair.Value;
			floor.Floor = null;
		}

		foreach (var pair in buildingObjects) {
			var building = pair.Value;
			building.Building = null;
		}
	}


	private Dictionary<GridCoord, FloorComponent> floorObjects = null;
	private Dictionary<GridCoord, BuildingComponent> buildingObjects = null;

	private void Awake() {
		var floorPrefabs = new Dictionary<FloorType, GameObject>();
		floorPrefabs[FloorType.Grass] = FloorGrass;
		floorPrefabs[FloorType.Soil] = FloorSoil;
		FloorComponent.FloorTypePrefabs = floorPrefabs;

		var buildingPrefabs = new Dictionary<BuildingType, GameObject>();
		buildingPrefabs[BuildingType.Tree] = BuildingTree;
		buildingPrefabs[BuildingType.Stump] = BuildingStump;
		BuildingComponent.BuildingTypePrefabs = buildingPrefabs;

		floorObjects = new Dictionary<GridCoord, FloorComponent>();
		buildingObjects = new Dictionary<GridCoord, BuildingComponent>();
	}	

	private FloorComponent CreateFloor(GridCoord coord) {
		var floorObject = Instantiate(PrefabFloor);
		GridPositionComponent.AddToObject(floorObject, coord);
		var floorComponent = FloorComponent.AddToObject(floorObject, null);
		floorObjects.Add(coord, floorComponent);
		return floorComponent;
	}

	private BuildingComponent CreateBuilding(GridCoord coord) {
		var buildingObject = Instantiate(PrefabBuilding);
		GridPositionComponent.AddToObject(buildingObject, coord);
		var buildingComponent = BuildingComponent.AddToObject(buildingObject, null);
		buildingObjects.Add(coord, buildingComponent);
		return buildingComponent;
	}
}
