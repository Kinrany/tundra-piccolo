﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridPositionComponent : MonoBehaviour {

	public GridCoord? Coord {
		get {
			return _coord;
		}
		set {
			_coord = value;
			UpdateTransform();
		}
	}

	public static GridPositionComponent AddToObject(GameObject obj, GridCoord? coord = null) {
		var component = obj.AddComponent<GridPositionComponent>();
		component.Coord = coord;
		return component;
	}


	private GridCoord? _coord = null;

	private void UpdateTransform() {
		if (_coord == null) {
			transform.position = new Vector3(0, 1000, 0);
		}
		else {
			var offsetY = new Vector3(0, transform.position.y, 0);
			transform.position = ((GridCoord)_coord).AsVector3() + offsetY;
		}
	}
}
