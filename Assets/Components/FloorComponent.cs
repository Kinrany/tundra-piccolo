﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorComponent : MonoBehaviour {

	public static Dictionary<FloorType, GameObject> FloorTypePrefabs;
	
	public Floor? Floor {
		get {
			return _floor;
		}
		set {
			_floor = value;
			UpdateModel();
		}
	}

	public static FloorComponent AddToObject(GameObject obj, Floor? floor) {
		var floorComponent = obj.AddComponent<FloorComponent>();
		floorComponent.Floor = floor;
		return floorComponent;
	}


	private Floor? _floor = null;

	private void UpdateModel() {
		const string MODEL_OBJECT_NAME = "Model";

		// destroy old model if present
		var oldModelTransform = transform.Find(MODEL_OBJECT_NAME);
		if (oldModelTransform != null) {
			Destroy(oldModelTransform.gameObject);
		}

		// if tile isn't empty, create new model
		if (_floor != null) {
			var newModelPrefab = FloorTypePrefabs[((Floor)_floor).Type];
			var newModelObject = Instantiate(newModelPrefab, gameObject.transform);
			newModelObject.name = MODEL_OBJECT_NAME;
		}
	}
}
