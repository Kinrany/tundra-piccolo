﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	public float Speed = 5;

	private Dictionary<KeyCode, Vector3> Keys = new Dictionary<KeyCode, Vector3>() {
		{KeyCode.UpArrow,    new Vector3( 0, 0,  1)},
		{KeyCode.W,          new Vector3( 0, 0,  1)},
		{KeyCode.DownArrow,  new Vector3( 0, 0, -1)},
		{KeyCode.S,          new Vector3( 0, 0, -1)},
		{KeyCode.LeftArrow,  new Vector3(-1, 0,  0)},
		{KeyCode.A,          new Vector3(-1, 0,  0)},
		{KeyCode.RightArrow, new Vector3( 1, 0,  0)},
		{KeyCode.D,          new Vector3( 1, 0,  0)}
	};

	private void FixedUpdate() {
		var transform = GetComponent<Transform>();
		var rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
		var distance = Speed * Time.fixedDeltaTime;

		foreach (var pair in Keys) {
			var keyCode = pair.Key;
			var direction = pair.Value;
			if (Input.GetKey(keyCode)) {
				transform.position += rotation * direction * distance;
			}
		}
	}
}
