﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectionManager : MonoBehaviour {

	public GameObject SelectionGreenPrefab;
	public GameObject SelectionOrangePrefab;
	public GameObject SelectionRedPrefab;

	public SelectionComponent Green { get; private set; }
	public SelectionComponent Orange { get; private set; }
	public SelectionComponent Red { get; private set; }

	private void Awake() {
		Green = Instantiate(SelectionGreenPrefab).GetComponent<SelectionComponent>();
		Orange = Instantiate(SelectionOrangePrefab).GetComponent<SelectionComponent>();
		Red = Instantiate(SelectionRedPrefab).GetComponent<SelectionComponent>();
	}

	private void Update() {
		GridCoord? greenCoord = null;
		GridCoord? orangeCoord = Orange.Coord;

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit)) {

			GameObject gameObject = hit.collider.gameObject;
			FloorComponent floor = gameObject.GetComponent<FloorComponent>();
			GridPositionComponent gridPos = gameObject.GetComponent<GridPositionComponent>();

			if (floor != null) {
				greenCoord = gridPos.Coord;
			}

			if (Input.GetKeyDown(KeyCode.Mouse0)) {
				orangeCoord = (floor == null) ? null : gridPos.Coord;
			}

		}
		
		Green.Coord = greenCoord;
		Orange.Coord = orangeCoord;
	}
}
