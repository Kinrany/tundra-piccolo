﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionComponent : MonoBehaviour {

	public float Elevation = 0.01f;

	public GridCoord? Coord {
		get {
			return _coord;
		}
		set {
			SetCoord(value);
		}
	}

	public FloorComponent FloorComponent {
		get {
			if (_coord == null) {
				return null;
			}

			return MapManager.GetFloor((GridCoord)_coord);
		}
	}

	public BuildingComponent BuildingComponent {
		get {
			if (_coord == null) {
				return null;
			}
			
			return MapManager.GetBuilding((GridCoord)_coord);
		}
	}

	public void SetCoord(GridCoord? coord) {
		_coord = coord;

		if (coord == null) {
			gameObject.SetActive(false);
		}
		else {
			gameObject.SetActive(true);
			transform.position = ((GridCoord)coord).AsVector3();
			transform.position += new Vector3(0, Elevation, 0);
		}
	}


	private GridCoord? _coord = null;

	private MapManager MapManager;

	private void Awake() {
		MapManager = FindObjectOfType<MapManager>();
	}
}
