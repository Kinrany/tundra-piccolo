﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Floor {
	public Floor(FloorType type) {
		this.Type = type;
	}

	public FloorType Type;
}

public enum FloorType {
	Grass,
	Soil
}