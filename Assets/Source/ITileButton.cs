﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITileButton {
	KeyCode Key { get; }
	string Text { get; }
	void Action(GridPositionComponent gridPosition);
	bool Filter(GridPositionComponent gridPosition);
}