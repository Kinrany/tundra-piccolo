﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct GridCoord {
	public int X;
	public int Y;

	public GridCoord(int x, int y) {
		this.X = x;
		this.Y = y;
	}

	public Vector3 AsVector3() {
		return new Vector3(this.X, 0, this.Y);
	}

	public static explicit operator GridCoord(Vector3 vect) {
		return new GridCoord((int)Math.Floor(vect.x), (int)Math.Floor(vect.z));
	}

	public override string ToString() {
		return String.Format("({0}, {1})", X, Y);
	}
}
