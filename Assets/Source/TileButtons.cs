﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileButtons : IEnumerable<ITileButton> {
	
	public TileButtons() {
		_buttons = new HashSet<ITileButton>() {
			new PlantTreeButton(),
			new CutTreeButton()
		};
	}

	public IEnumerator<ITileButton> GetEnumerator() {
		return _buttons.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator() {
		return _buttons.GetEnumerator();
	}

	private HashSet<ITileButton> _buttons;
}
