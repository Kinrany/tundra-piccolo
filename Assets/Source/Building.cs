﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Building {
	public Building(BuildingType type) {
		this.Type = type;
	}

	public BuildingType Type;
}

public enum BuildingType {
	Tree,
	Stump
}