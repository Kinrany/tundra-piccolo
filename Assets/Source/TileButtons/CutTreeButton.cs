﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutTreeButton : ITileButton {
	public KeyCode Key {
		get {
			return KeyCode.X;
		}
	}

	public string Text {
		get {
			return "cut tree";
		}
	}

	public void Action(GridPositionComponent gridPosition) {
		var tree = gridPosition.GetComponent<BuildingComponent>();
		tree.Building = new Building(BuildingType.Stump);
	}

	public bool Filter(GridPositionComponent gridPosition) {
		var building = gridPosition.GetComponent<BuildingComponent>();
		if (building.Building == null) {
			return false;
		}
		return ((Building)(building.Building)).Type == BuildingType.Tree;
	}
}
