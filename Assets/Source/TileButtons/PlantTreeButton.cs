﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PlantTreeButton : ITileButton {

	public KeyCode Key {
		get {
			return KeyCode.Z;
		}
	}

	public string Text {
		get {
			return "plant a tree";
		}
	}

	public void Action(GridPositionComponent gridPosition) {
		var building = gridPosition.GetComponent<BuildingComponent>();
		building.Building = new Building(BuildingType.Tree);
	}

	public bool Filter(GridPositionComponent gridPosition) {
		var building = gridPosition.GetComponent<BuildingComponent>();
		return building.Building == null;
	}
}

